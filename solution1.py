size = 5


def up():
    for i in range(size - 1, -1, -1):
        for j in range(i):
            print(" ", end="")
        for j in range(2 * size - 1 - i * 2):
            print("*", end="")
        print()


def down():
    for i in range(size):
        for j in range(i):
            print(" ", end="")
        for j in range(2 * size - 1 - i * 2):
            print("*", end="")
        print()


up()
down()
