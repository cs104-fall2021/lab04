size = 5


def up2():
    for i in range(size):
        for j in range(i):
            print(" ", end="")
        for j in range(2 * size - 1 - i * 2):
            if j == 0:
                print("\\", end="")
            if j == 2 * size - 1 - i * 2 - 1:
                print("/", end="")
            else:
                print("o", end="")
        print()


def down2():
    for i in range(size - 1, -1, -1):
        for j in range(i):
            print(" ", end="")
        for j in range(2 * size - 1 - i * 2):
            if j == 0:
                print("/", end="")
            if j == 2 * size - 1 - i * 2 - 1:
                print("\\", end="")
            else:
                print("o", end="")
        print()


up2()
down2()
