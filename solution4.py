PI = 3.14159


def calculate_area(radius):
    print("The area of the sphere is", 4 * PI * radius ** 2)


def calculate_volume(radius):
    print("The volume of the sphere is", 4 / 3 * PI * radius ** 3)


radius = 78
# Surface Area of a sphere = 4 times the area of its great circle = 4 * π * Radius ^ 2
# = 4 * π * 78.0^2 =76453.8 square units
calculate_area(radius)
# Volume of a sphere = (4/3) * π * Radius ^ 3 =  (4/3) * π * 78.0 ^ 3 =  1987798.77 cubic units
calculate_volume(radius)
