def factorial(n):
    result = 1
    for i in range(2, n + 1):
        result *= i
    print("Factorial of", n, "is", result)
    return result


def combination(n, k):
    result = factorial(n) / (factorial(n - k) * factorial(k))
    print(str(k) + "-combination of an " + str(n) + "-element set is " + str(result))


# One can compute the number of five-card hands possible from a standard fifty-two card deck as 2598960
combination(52, 5)
