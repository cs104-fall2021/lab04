import random

computer_picked_random_integer = random.randint(1, 100)

player_guess = 0
number_of_guesses = 0
while computer_picked_random_integer != player_guess:
    player_guess = int(input("Please enter a guess: "))
    number_of_guesses += 1

    if player_guess > computer_picked_random_integer:
        print("Too high")
    elif player_guess < computer_picked_random_integer:
        print("Too low")
    else:
        print("Correct!")

print("The number of guesses is", number_of_guesses)
print("The computer picked random integer is ", computer_picked_random_integer)
